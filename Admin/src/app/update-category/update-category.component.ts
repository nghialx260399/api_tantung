import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';
import { NotificationService } from '../shared/notification.service';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {
  id: any;
  category: any;
  categoryForm: FormGroup;
  constructor(private route: ActivatedRoute, private categoryService: CategoryService, private fb: FormBuilder,private router:Router,
    private noticeService:NotificationService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getCategoryById();
    this.categoryForm = this.fb.group({
      id:"",
      name:['', Validators.required],
      createdOn:['', Validators.required],
      updatedOn:['', Validators.required],
    });
   }

  ngOnInit(): void {
  }

  getCategoryById() {
    this.categoryService.getCategoryById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.category = res.body;
        this.categoryForm.controls['id'].setValue(this.category.id);
        this.categoryForm.controls['name'].setValue(this.category.name);
        this.categoryForm.controls['createdOn'].setValue(this.category.createdOn);
        this.categoryForm.controls['updatedOn'].setValue(this.category.updatedOn);
      }
      else {
        console.log('get category by id failed!');
      }

    })
  }

  Update() {
    if(!this.categoryForm.invalid){
      this.categoryService.updateCategory(this.categoryForm.value).subscribe(res=>{
        if(res.status==200){
          this.noticeService.show('success', 'Update successfully');
          this.router.navigateByUrl('/admin/category');
        }
        else{
          this.noticeService.show('error', 'Update fail');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }
}
