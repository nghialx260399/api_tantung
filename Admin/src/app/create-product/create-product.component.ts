import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';
import { NotificationService } from '../shared/notification.service';
import { ProductService } from '../shared/product.service';
import { image } from './image.const';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  product: any;
  productForm: FormGroup;
  categories:any;
  imageSource : any;
  private base64textString:any;

  constructor(private route: ActivatedRoute, private productService: ProductService, private fb: FormBuilder,private router:Router,
    private categoryService:CategoryService, private sanitizer: DomSanitizer, private noticeService:NotificationService) { 
    this.productForm = this.fb.group({
      name:['', Validators.required],
      quantity:['',Validators.required],
      price:['', Validators.required],
      size:['', Validators.required],
      description:['', Validators.required],
      categoryId:['', Validators.required],
      color:['', Validators.required],
      promotionPrice:['', Validators.required],
      image:['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.getCategories();
    this.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpeg;base64, ${image}`);
  }

  handleFileSelect(evt:any){
    var files = evt.target.files;
    var file = files[0];
  
  if (files && file) {
      var reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
  }
}

_handleReaderLoaded(readerEvt:any) {
   var binaryString = readerEvt.target.result;
          this.base64textString= btoa(binaryString);
          this.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpeg;base64, ${this.base64textString}`);
          this.productForm.controls['image'].setValue(this.base64textString);
          console.log(this.base64textString);
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  Create(){
    if(!this.productForm.invalid){
      this.productService.addProduct(this.productForm.value).subscribe(res=>{
        if(res.status==200){
          this.noticeService.show('success', 'Create successfully');
          this.router.navigateByUrl('/admin/product');
        }
        else{
          this.noticeService.show('error', 'Create fail');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

  get name(){
    return this.productForm.get('name');
  }
  get price(){
    return this.productForm.get('price');
  }

  get size(){
    return this.productForm.get('size');
  }

  get image(){
    return this.productForm.get('image');
  }

  get description(){
    return this.productForm.get('description');
  }

  get categoryId(){
    return this.productForm.get('categoryId');
  }

  get color(){
    return this.productForm.get('color');
  }

  get promotionPrice(){
    return this.productForm.get('promotionPrice');
  }
  get quantity(){
    return this.productForm.get('quantity');
  }
}
