import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { category, product } from './model';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient:HttpClient) { }

  getCategories(): Observable<HttpResponse<category[]>> {
    const apiUrl = 'https://localhost:44305/api/Categories/GetAll';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<category[]>(apiUrl, options);
  }

  getCategoiesByName(name:string): Observable<HttpResponse<category[]>> {
    const apiUrl = `https://localhost:44305/api/Categories/SearchByName/${name}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<category[]>(apiUrl, options);
  }

  getCategoryById(id:number): Observable<HttpResponse<category[]>> {
    const apiUrl = `https://localhost:44305/api/Categories/GetById/${id}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<category[]>(apiUrl, options);
  }
  updateCategory(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= `https://localhost:44305/api/Categories/Update`;
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }

  addCategory(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= 'https://localhost:44305/api/Categories/Create';
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.post(apiUrl,data, options);

  }

  deleteCategory(id: string) {
    const apiUrl = `https://localhost:44305/api/Categories/Delete/${id}`;
    return this.httpClient.delete(apiUrl, { observe: 'response' });
  }
}
