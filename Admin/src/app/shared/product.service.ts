import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { product } from './model';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient:HttpClient) { }

  getProducts(): Observable<HttpResponse<product[]>> {
    const apiUrl = 'https://localhost:44305/api/Products/GetAll';

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsByCategoryId(id:number): Observable<HttpResponse<product[]>> {
    const apiUrl = `https://localhost:44380/api/product?categoryId=${id}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsById(id:number): Observable<HttpResponse<product[]>> {

    const apiUrl = `https://localhost:44305/api/Products/GetById/${id}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  getProductsByName(name:string): Observable<HttpResponse<product[]>> {
    const apiUrl = `https://localhost:44305/api/Products/SearchByName/${name}`;

    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      observe: 'response' as const
    }
    return this.httpClient.get<product[]>(apiUrl, options);
  }

  updateProduct(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= `https://localhost:44305/api/Products/Update`;
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.put(apiUrl,data, options);

  }

  addProduct(formData:any){
    var data= JSON.stringify(formData);
    const apiUrl= 'https://localhost:44305/api/Products/Create';
    var options={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      }),
      observe:'response' as const
    }
    return this.httpClient.post(apiUrl,data, options);

  }

  deleteProduct(id: string) {
    const apiUrl = `https://localhost:44305/api/Products/Delete/${id}`;
    return this.httpClient.delete(apiUrl, { observe: 'response' });
  }
}
