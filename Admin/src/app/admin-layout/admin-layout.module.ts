import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminLayoutComponent } from './admin-layout.component';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NotifierModule } from 'angular-notifier';
import { customNotifierOptions } from '../shared/notification-config';
import { ProductComponent } from '../product/product.component';
import { CategoryComponent } from '../category/category.component';
import { EditComponent } from '../edit/edit.component';
import { CreateProductComponent } from '../create-product/create-product.component';
import { AddCategoryComponent } from '../add-category/add-category.component';
import { UpdateCategoryComponent } from '../update-category/update-category.component';
import { OrderComponent } from '../order/order.component';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { RouterModule } from '@angular/router';
import { AdminLayoutRoutingModule, routes } from './admin-layout-routing.module';
import { HomeComponent } from '../home/home.component';
import { DetailOrderComponent } from '../detail-order/detail-order.component';


@NgModule({
  declarations: [
    ProductComponent,
    CategoryComponent,
    EditComponent,
    CreateProductComponent,
    AddCategoryComponent,
    UpdateCategoryComponent,
    OrderComponent,
    ConfirmationDialogComponent,
    HomeComponent,
    DetailOrderComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AdminLayoutRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NotifierModule.withConfig(customNotifierOptions),
    NgbModule,
  ]
})
export class AdminLayoutModule { }
