import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddCategoryComponent } from '../add-category/add-category.component';
import { CategoryComponent } from '../category/category.component';
import { CreateProductComponent } from '../create-product/create-product.component';
import { DetailOrderComponent } from '../detail-order/detail-order.component';
import { EditComponent } from '../edit/edit.component';
import { HomeComponent } from '../home/home.component';
import { OrderComponent } from '../order/order.component';
import { ProductComponent } from '../product/product.component';
import { UpdateCategoryComponent } from '../update-category/update-category.component';
import { AdminLayoutComponent } from './admin-layout.component';

export const routes: Routes = [
  {path: 'admin', component: AdminLayoutComponent, children:[
    {path:'home',component:HomeComponent},
    {path:'product',component:ProductComponent},
    {path:'category',component:CategoryComponent},
    {path:'edit-product/:id', component: EditComponent},
    {path:'create-product', component: CreateProductComponent},
    {path:'create-category', component: AddCategoryComponent},
    {path:'edit-category/:id', component: UpdateCategoryComponent},
    {path:'order', component: OrderComponent},
    {path:'detail-order/:id',component:DetailOrderComponent},
  ]}
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminLayoutRoutingModule { }
