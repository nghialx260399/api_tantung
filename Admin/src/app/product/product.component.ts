import { Component, OnInit } from '@angular/core';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { NotificationService } from '../shared/notification.service';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  products:any; cp:number=1;
  notFound = false;
  constructor(private productService:ProductService, private noticeService:NotificationService,
    private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit(): void {
    this.getProducts();
  }

  getProducts(){
    this.productService.getProducts().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.products=res.body;
       this.notFound = false;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  deleteProduct(id:string){
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete ?')
      .then((confirmed) => {
        if(confirmed){
          this.productService.deleteProduct(id).subscribe(res=>{
            if(res.status==200){
              this.noticeService.show('success', 'Delete successfully');
              this.getProducts();
            }
            else{
              this.noticeService.show('error', 'Delete fail');
            }
          })
        }
      });
}

getProductsByName(name:string){
  if(name == ''){
    this.productService.getProducts().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.products=res.body;
       this.notFound = false;
       this.cp = 1;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }
  else{
    // this.products = this.products.filter((x:any) => x.name.toLowerCase().includes(name.toLowerCase()));
    // if(this.products.length == 0)
    //   this.notFound = true;
    this.productService.getProductsByName(name).subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.products=res.body;
       this.cp = 1;
       if(this.products.length == 0)
          this.notFound = true;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }
}
}
