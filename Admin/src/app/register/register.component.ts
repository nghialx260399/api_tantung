import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { NotificationService } from '../shared/notification.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css',  './res1.component.css', './res2.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm:FormGroup;

  constructor(private fb:FormBuilder,private authService:AuthService,private route:Router, private notifi:NotificationService) {
    this.registerForm= this.fb.group({
      email:[''],
      username:[''],
      password:['']
    })
   }

  ngOnInit(): void {
  }

  submit(){
    this.authService.register(this.registerForm.value).subscribe(res=>{
      if(res.status == 200){
        this.notifi.show('success',"Register is successfully");
        this.route.navigateByUrl('/login');
      }
      else{
        this.notifi.show('error',"Register is failed");
      }
    })
  }

}
