import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from '../shared/category.service';
import { NotificationService } from '../shared/notification.service';
import { ProductService } from '../shared/product.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id: any;
  product: any;
  productForm: FormGroup;
  imageSource : any;
  private base64textString:any;
  categories:any;
  constructor(private route: ActivatedRoute, private productService: ProductService, private fb: FormBuilder,private router:Router,
    private sanitizer: DomSanitizer, private categoryService:CategoryService, private noticeService:NotificationService) { 
    this.id = this.route.snapshot.paramMap.get('id');
    this.getProductById();
    this.productForm = this.fb.group({
      id:"",
      name:['', Validators.required],
      quantity:['',Validators.required],
      price:['', Validators.required],
      size:['', Validators.required],
      description:['', Validators.required],
      categoryId:['', Validators.required],
      color:['', Validators.required],
      promotionPrice:['', Validators.required],
      image:['', Validators.required],
      createdOn:['', Validators.required],
      updatedOn:['', Validators.required],
    });
  }

  ngOnInit(): void {
    this.getCategories();
  }

  handleFileSelect(evt:any){
    var files = evt.target.files;
    var file = files[0];
  
  if (files && file) {
      var reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
  }
}

_handleReaderLoaded(readerEvt:any) {
   var binaryString = readerEvt.target.result;
          this.base64textString= btoa(binaryString);
          this.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpeg;base64, ${this.base64textString}`);
          this.productForm.controls['image'].setValue(this.base64textString);
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  getProductById() {
    this.productService.getProductsById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.product = res.body;
        this.productForm.controls['id'].setValue(this.product.id);
        this.productForm.controls['name'].setValue(this.product.name);
        this.productForm.controls['quantity'].setValue(this.product.quantity);
        this.productForm.controls['price'].setValue(this.product.price);
        this.productForm.controls['promotionPrice'].setValue(this.product.promotionPrice);
        this.productForm.controls['image'].setValue(this.product.image);
        this.productForm.controls['description'].setValue(this.product.description);
        this.productForm.controls['categoryId'].setValue(this.product.categoryId);
        this.productForm.controls['size'].setValue(this.product.size);
        this.productForm.controls['color'].setValue(this.product.color);
        this.productForm.controls['createdOn'].setValue(this.product.createdOn);
        this.productForm.controls['updatedOn'].setValue(this.product.updatedOn);

        this.base64textString =  this.productForm.get('image')?.value;
        this.imageSource = this.sanitizer.bypassSecurityTrustResourceUrl(`data:image/jpeg;base64, ${this.base64textString}`);
      }
      else {
        console.log('get product by id failed!');
      }

    })
  }

  Update() {
    if(!this.productForm.invalid){
      this.productService.updateProduct(this.productForm.value).subscribe(res=>{
        if(res.status==200){
          this.noticeService.show('success','Update successfully');
          this.router.navigateByUrl('/admin/product');
        }
        else{
          this.noticeService.show('error', 'Update fail');
        }
       })
    }
    else{
      alert('Validate form error');
    }

  }

  get name(){
    return this.productForm.get('name');
  }
  get price(){
    return this.productForm.get('price');
  }

  get size(){
    return this.productForm.get('size');
  }

  get image(){
    return this.productForm.get('image');
  }

  get description(){
    return this.productForm.get('description');
  }

  get categoryId(){
    return this.productForm.get('categoryId');
  }

  get color(){
    return this.productForm.get('color');
  }

  get promotionPrice(){
    return this.productForm.get('promotionPrice');
  }
  get quantity(){
    return this.productForm.get('quantity');
  }
}
