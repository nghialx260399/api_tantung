import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../shared/notification.service';
import { OrderService } from '../shared/order.service';

@Component({
  selector: 'app-detail-order',
  templateUrl: './detail-order.component.html',
  styleUrls: ['./detail-order.component.css']
})
export class DetailOrderComponent implements OnInit {

  id: any;
  order: any;
  cp:number = 1;
  constructor(private route: ActivatedRoute, private orderService: OrderService,private noticeService:NotificationService) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getOrderById();
   }

  ngOnInit(): void {
  }

  getOrderById() {
    this.orderService.getOrderById(this.id).subscribe(res => {
      if (res.status == 200) {
        this.order = res.body;
        console.log(this.order.status);
      }
      else {
        console.log('get category by id failed!');
      }

    })
  }

  updateOrder(id:any){
    this.orderService.updateOrder(id).subscribe(res=>{
    console.log(res);
      if(res.status==200){
        this.noticeService.show('success', 'Approve is successfuly');
        this.getOrderById();
      }
      else{
        this.noticeService.show('error', 'Approve is failed');
      }
    })
  }

  rejectOrder(id:any){
    this.orderService.rejectOrder(id).subscribe(res=>{
    console.log(res);
      if(res.status==200){
        this.noticeService.show('success', 'Reject is successfuly');
        this.getOrderById();
      }
      else{
        this.noticeService.show('error', 'Reject is failed');
      }
    })
  }
}
