import { Component, OnInit } from '@angular/core';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { CategoryService } from '../shared/category.service';
import { NotificationService } from '../shared/notification.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories:any;
  cp:number=1;
  notFound = false;
  constructor(private categoryService:CategoryService, private noticeService:NotificationService,
    private confirmationDialogService: ConfirmationDialogService) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(){
    this.categoryService.getCategories().subscribe(res=>{
      console.log(res);
     if(res.status==200){
       this.categories=res.body;
       this.notFound = false;
     }
     else{
       alert('Get data from serve  failed');
     }
    })
  }

  deleteCategory(id:string){
    this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete ?')
      .then((confirmed) => {
        if(confirmed){
          this.categoryService.deleteCategory(id).subscribe(res=>{
            if(res.status==200){
              this.noticeService.show('success', 'Delete successfully');
              this.getCategories();
            }
            else{
              this.noticeService.show('error', 'Delete fail');
            }
          })
        }
      });
  }

  getCategoryByName(name:string){
    if(name == ''){
      this.categoryService.getCategories().subscribe(res=>{
        console.log(res);
       if(res.status==200){
         this.categories=res.body;
         this.notFound = false;
         this.cp = 1;
       }
       else{
         alert('Get data from serve  failed');
       }
      })
    }
    else{
      // this.products = this.products.filter((x:any) => x.name.toLowerCase().includes(name.toLowerCase()));
      // if(this.products.length == 0)
      //   this.notFound = true;
      this.categoryService.getCategoiesByName(name).subscribe(res=>{
        console.log(res);
       if(res.status==200){
         this.categories=res.body;
         this.cp = 1;
         if(this.categories.length == 0)
            this.notFound = true;
       }
       else{
         alert('Get data from serve  failed');
       }
      })
    }
  }
}
