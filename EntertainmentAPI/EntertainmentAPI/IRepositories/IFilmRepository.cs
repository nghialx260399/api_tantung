﻿using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;

namespace EntertainmentAPI.IRepositories
{
    public interface IFilmRepository : IGenericRepository<Films>
    {
    }
}
