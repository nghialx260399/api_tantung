﻿using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Data.Tung;
using EntertainmentAPI.Infastructures;

namespace EntertainmentAPI.IRepositories
{
    public interface IFavoritemRepository : IGenericRepository<Favoritem>
    {
        void DeleteFavorite(string userId, int filmId);
    }
}
