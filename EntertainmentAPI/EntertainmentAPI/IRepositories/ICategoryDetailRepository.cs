﻿using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;

namespace EntertainmentAPI.IRepositories
{
    public interface ICategoryDetailRepository : IGenericRepository<CategoriesDetails>
    {
        void DeleteByFilmId(IList<CategoriesDetails> list);
    }

   
}
