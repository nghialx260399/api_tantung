﻿using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.ViewModels.Films;

namespace EntertainmentAPI.IRepositories
{
    public interface IFavoritefRepository : IGenericRepository<Favoritesf>
    {
        void DeleteFavorite(string userId, int filmId);
    }
}
