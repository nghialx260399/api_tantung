using API.Data;
using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;
using EntertainmentAPI.Repositories;
using EntertainmentAPI.Services.CategoriesF;
using EntertainmentAPI.Services.CategoriesM;
using EntertainmentAPI.Services.Episodes;
using EntertainmentAPI.Services.Films;
using EntertainmentAPI.Services.Musics;
using EntertainmentAPI.Services.Users;
using EntertainmentAPI.ViewModels.AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
    options.LogTo(Console.WriteLine);
});

builder.Services.AddIdentity<AppUser, IdentityRole>(option =>
{
    option.Password.RequireDigit = false;
    option.Password.RequireLowercase = false;
    option.Password.RequireUppercase = false;
    option.Password.RequireNonAlphanumeric = false;
    option.SignIn.RequireConfirmedAccount = false;
})
.AddEntityFrameworkStores<ApplicationDbContext>();

string issuer = builder.Configuration.GetValue<string>("Jwt:Issuer");
string signingKey = builder.Configuration.GetValue<string>("Jwt:Key");
byte[] signingKeyBytes = System.Text.Encoding.UTF8.GetBytes(signingKey);

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = false;
    options.SaveToken = true;
    options.TokenValidationParameters = new TokenValidationParameters()
    {
        ValidateIssuer = false,
        ValidIssuer = issuer,
        ValidateAudience = false,
        ValidAudience = issuer,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ClockSkew = System.TimeSpan.Zero,
        IssuerSigningKey = new SymmetricSecurityKey(signingKeyBytes)
    };
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Entertainment.Api", Version = "v1" });
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = @"JWT Authorization header using the Bearer scheme. \r\n\r\n
                      Enter 'Bearer' [space] and then your token in the text input below.
                      \r\n\r\nExample: 'Bearer 12345abcdef'",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });

    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                  {
                     {
                          new OpenApiSecurityScheme
                            {
                              Reference = new OpenApiReference
                              {
                                  Type = ReferenceType.SecurityScheme,
                                  Id = "Bearer"
                               },
                              Scheme = "oauth2",
                              Name = "Bearer",
                                 In = ParameterLocation.Header,
                     },
                        new List<string>()
                      }
                 });
});

// Mapper

builder.Services.AddAutoMapper(typeof(MappingProfile));

// Repository
builder.Services.AddTransient<IUnitOfWork, UnitOfWork>();
builder.Services.AddTransient<ICategoryDetailRepository, CategoryDetailRepository>();    
builder.Services.AddTransient<ICategoryfRepository, CategoryfRepository>();    
builder.Services.AddTransient<ICategorymRepository, CategorymRepository>();    
builder.Services.AddTransient<IEpisodesRepository, EpisodesRepository>();    
builder.Services.AddTransient<IFavoritefRepository, FavoritefRepository>();    
builder.Services.AddTransient<IFavoritemRepository, FavoritemRepository>();    
builder.Services.AddTransient<IFilmRepository, FilmRepository>();    
builder.Services.AddTransient<IMusicRepository, MusicRepository>();

// Service
builder.Services.AddTransient<ICategoriesFService, CategoriesFService>();
builder.Services.AddTransient<ICategoriesMService, CategoriesMService>();
builder.Services.AddTransient<IEpisodesService, EpisodesService>();
builder.Services.AddTransient<IFilmService, FilmService>();
builder.Services.AddTransient<IMusicService, MusicService>();
builder.Services.AddTransient<IUserService, UserService>();


var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    var loggerFactory = services.GetRequiredService<ILoggerFactory>();
    try
    {
        var context = services.GetRequiredService<ApplicationDbContext>();
        var userManager = services.GetRequiredService<UserManager<AppUser>>();
        var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
        await ContextSeed.SeedRolesAsync(userManager, roleManager);
        await ContextSeed.SeedSuperAdminAsync(userManager, roleManager);
    }
    catch (Exception ex)
    {
        var logger = loggerFactory.CreateLogger<Program>();
        logger.LogError(ex, "An error occurred seeding the DB.");
    }
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.UseCors(x =>
{
    x.AllowAnyMethod();
    x.AllowAnyHeader();
    x.AllowAnyOrigin();
});

app.MapControllers();

app.Run();
