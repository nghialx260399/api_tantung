﻿using EntertainmentAPI.Services.Films;
using EntertainmentAPI.ViewModels.Films;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EntertainmentAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TanFilmsController : ControllerBase
    {
        private readonly IFilmService categoryService;

        public TanFilmsController(IFilmService categoryService)
        {
            this.categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await categoryService.GetAll());
        }

        [HttpGet("{filmName}")]
        public async Task<IActionResult> SearchByNam(string filmName)
        {
            return Ok(await categoryService.SearchByName(filmName));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var category = await categoryService.GetById(id);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpGet("{position}")]
        public async Task<IActionResult> GetByPosition(string position)
        {
            var category = await categoryService.GetFilmsByPosition(position);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpGet("{categoryId}")]
        public async Task<IActionResult> GetByCategoryId(int categoryId)
        {
            var category = await categoryService.GetByCategoryId(categoryId);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetFavorite(string userId)
        {
            var category = await categoryService.GetFilmsByFavorite(userId);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateFilmVm categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.Create(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> CreateFavorite(FavoriteF categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.CreateFavorite(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Update(CreateFilmVm categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.Update(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{userId}/{filmId}")]
        public async Task<IActionResult> DeleteFavorite(string userId, int filmId)
        {
            bool isSuccess = await categoryService.DeleteFavorite(userId, filmId);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            bool isSuccess = await categoryService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }
    }
}
