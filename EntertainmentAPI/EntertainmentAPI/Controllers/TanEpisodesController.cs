﻿using EntertainmentAPI.Services.Episodes;
using EntertainmentAPI.ViewModels.Episodes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EntertainmentAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TanEpidosesController : ControllerBase
    {
        private readonly IEpisodesService categoryService;

        public TanEpidosesController(IEpisodesService categoryService)
        {
            this.categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await categoryService.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var category = await categoryService.GetById(id);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpGet("{filmId}")]
        public async Task<IActionResult> GetByFilmId(int filmId)
        {
            var category = await categoryService.GetByFilmId(filmId);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ViewEpisodes categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.Create(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Update(ViewEpisodes categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.Update(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            bool isSuccess = await categoryService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }
    }
}
