﻿using EntertainmentAPI.Services.Musics;
using EntertainmentAPI.ViewModels.Musics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EntertainmentAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TungMusicsController : ControllerBase
    {
        private readonly IMusicService categoryService;

        public TungMusicsController(IMusicService categoryService)
        {
            this.categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await categoryService.GetAll());
        }

        [HttpGet("{musicName}")]
        public async Task<IActionResult> SearchByName(string musicName)
        {
            return Ok(await categoryService.SearchByName(musicName));
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> GetFavorite(string userId)
        {
            var category = await categoryService.GetMusicsByFavorite(userId);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpGet("{author}")]
        public async Task<IActionResult> GetByAuthor(string author)
        {
            var category = await categoryService.GetByAuthor(author);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var category = await categoryService.GetById(id);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpGet("{categoryId}")]
        public async Task<IActionResult> GetByCategoryId(int categoryId)
        {
            var category = await categoryService.GetByCategoryId(categoryId);

            if (category != null)
                return Ok(category);

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ViewMusic categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.Create(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> CreateFavorite(FavoriteM categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.CreateFavorite(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Update(ViewMusic categoryVm)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");

            bool isSuccess = await categoryService.Update(categoryVm);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{userId}/{musicId}")]
        public async Task<IActionResult> DeleteFavorite(string userId, int musicId)
        {
            bool isSuccess = await categoryService.DeleteFavorite(userId, musicId);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            bool isSuccess = await categoryService.Delete(id);

            if (isSuccess)
                return Ok(isSuccess);

            return BadRequest();
        }
    }
}
