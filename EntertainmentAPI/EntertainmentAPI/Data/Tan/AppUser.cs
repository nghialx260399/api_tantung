﻿using EntertainmentAPI.Data.Tung;
using Microsoft.AspNetCore.Identity;

namespace EntertainmentAPI.Data.Tan
{
    public class AppUser:IdentityUser
    {
        public IEnumerable<Favoritesf>? Favoritesf { get; set; }

        public IEnumerable<Favoritem>? Favoritesm { get; set; }
    }
}
