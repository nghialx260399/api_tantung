﻿namespace EntertainmentAPI.Data.Tan
{
    public class Episodes:EntityBase.EntityBase
    {
        public string? Name { get; set; }

        public string? Source { get; set; }

        public int FilmsId { get; set; }

        public Films? Films { get; set; }
    }
}
