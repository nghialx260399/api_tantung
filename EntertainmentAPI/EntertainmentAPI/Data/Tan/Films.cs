﻿namespace EntertainmentAPI.Data.Tan
{
    public class Films:EntityBase.EntityBase
    {
        public string? Name { get; set; }

        public string? Author { get; set; }

        public string? Position { get; set; }

        public int? Year { get; set; }

        public int? View { get; set; }

        public float? Rate { get; set; }

        public byte[]? Image { get; set; }

        public bool? Status { get; set; }

        public int? TotalEP { get; set; }

        public IEnumerable<CategoriesDetails>? CategoriesDetails { get; set; }

        public IEnumerable<Favoritesf>? Favorites { get; set; }

        public IEnumerable<Episodes>? Episodes { get; set; }

    }
}
