﻿using EntertainmentAPI.Data.EntityBase;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Data.Tung;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace EntertainmentAPI.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Categoriesf> Categoriesf { get; set; }

        public DbSet<Categoriesm> Categoriesm { get; set; }

        public DbSet<CategoriesDetails> CategoriesDetails { get; set; }

        public DbSet<Episodes> Episodes { get; set; }

        public DbSet<Favoritesf> Favoritesfs { get; set; }

        public DbSet<Films> Films { get; set; }

        public DbSet<Favoritem> Favoritems { get; set; }

        public DbSet<Musics> Musics { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Films>()
                        .HasMany<Episodes>(c => c.Episodes)
                        .WithOne(p => p.Films)
                        .HasForeignKey(p => p.FilmsId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<CategoriesDetails>().HasKey(pt => new { pt.CategoryId, pt.FilmId });

            modelBuilder.Entity<Favoritesf>().HasKey(pt => new { pt.UserId, pt.FilmsId });

            modelBuilder.Entity<Categoriesm>()
                        .HasMany<Musics>(u => u.Musics)
                        .WithOne(c => c.Categoriesm)
                        .HasForeignKey(c => c.CategoryId)
                        .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Favoritem>().HasKey(pt => new { pt.UserId, pt.MusicId });

            base.OnModelCreating(modelBuilder);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            BeforSaveChanges();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void BeforSaveChanges()
        {
            var entities = ChangeTracker.Entries();
            foreach (var entity in entities)
            {
                var now = DateTime.Now;
                if (entity.Entity is IEntityBase asEntity)
                {
                    if (entity.State == EntityState.Added)
                    {
                        asEntity.CreatedOn = now;
                        asEntity.UpdatedOn = now;
                    }
                    if (entity.State == EntityState.Modified)
                    {
                        asEntity.UpdatedOn = now;
                    }
                }
            }
        }
    }
}
