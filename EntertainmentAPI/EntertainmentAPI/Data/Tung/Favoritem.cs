﻿using EntertainmentAPI.Data.Tan;

namespace EntertainmentAPI.Data.Tung
{
    public class Favoritem:EntityBase.EntityBase
    {
        public int MusicId { get; set; }

        public Musics? Musics { get; set; }

        public string UserId { get; set; }

        public AppUser? UserApp { get; set; }
    }
}
