﻿namespace EntertainmentAPI.Data.Tung
{
    public class Musics:EntityBase.EntityBase
    {
        public string? Name { get; set; }

        public byte[]? Image { get; set; }

        public string? Author { get; set; }

        public string? Source { get; set; }

        public string? Time { get; set; }

        public int CategoryId { get; set; }

        public Categoriesm? Categoriesm { get; set; }

        public IEnumerable<Favoritem>? Favoritesm { get; set; }
    }
}
