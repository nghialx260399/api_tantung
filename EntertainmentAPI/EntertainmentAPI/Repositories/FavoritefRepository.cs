﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;
using EntertainmentAPI.ViewModels.Films;

namespace EntertainmentAPI.Repositories
{
    public class FavoritefRepository : GenericRepository<Favoritesf>, IFavoritefRepository
    {
        public FavoritefRepository(ApplicationDbContext context) : base(context)
        {
        }

        public void DeleteFavorite(string userId, int filmId)
        {
            var favorite = this.DbSet.FirstOrDefault(x=>x.FilmsId == filmId && x.UserId == userId);

            if (favorite == null)
                throw new Exception();

            this.DbSet.Remove(favorite);
        }
    }
}
