﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tung;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Repositories
{
    public class MusicRepository : GenericRepository<Musics>, IMusicRepository
    {
        public MusicRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
