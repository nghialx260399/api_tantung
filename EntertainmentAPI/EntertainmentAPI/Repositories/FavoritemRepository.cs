﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tung;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Repositories
{
    public class FavoritemRepository : GenericRepository<Favoritem>, IFavoritemRepository
    {
        public FavoritemRepository(ApplicationDbContext context) : base(context)
        {
        }

        public void DeleteFavorite(string userId, int filmId)
        {
            var favorite = this.DbSet.FirstOrDefault(x => x.MusicId == filmId && x.UserId == userId);

            if (favorite == null)
                throw new Exception();

            this.DbSet.Remove(favorite);
        }
    }
}
