﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tung;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Repositories
{
    public class CategorymRepository : GenericRepository<Categoriesm>, ICategorymRepository
    {
        public CategorymRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
