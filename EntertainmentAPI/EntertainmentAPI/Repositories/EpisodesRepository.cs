﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Repositories
{
    public class EpisodesRepository : GenericRepository<Episodes>, IEpisodesRepository
    {
        public EpisodesRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
