﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Repositories
{
    public class CategoryDetailRepository : GenericRepository<CategoriesDetails>, ICategoryDetailRepository
    {
        public CategoryDetailRepository(ApplicationDbContext context) : base(context)
        {
        }

        public void DeleteByFilmId(IList<CategoriesDetails> list)
        {
            this.DbSet.RemoveRange(list);
        }
    }
}
