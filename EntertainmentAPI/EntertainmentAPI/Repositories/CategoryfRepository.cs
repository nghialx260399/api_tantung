﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Repositories
{
    public class CategoryfRepository : GenericRepository<Categoriesf>, ICategoryfRepository
    {
        public CategoryfRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
