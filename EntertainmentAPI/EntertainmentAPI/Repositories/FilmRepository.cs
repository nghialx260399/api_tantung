﻿using EntertainmentAPI.Data;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Repositories
{
    public class FilmRepository : GenericRepository<Films>, IFilmRepository
    {
        public FilmRepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
