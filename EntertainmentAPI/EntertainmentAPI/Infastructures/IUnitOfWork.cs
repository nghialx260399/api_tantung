﻿
using EntertainmentAPI.Data;
using EntertainmentAPI.IRepositories;

namespace EntertainmentAPI.Infastructures
{
    public interface IUnitOfWork:IDisposable
    {
        ICategoryDetailRepository CategoryDetailRepository { get; }

        ICategoryfRepository CategoryfRepository { get; }

        ICategorymRepository CategorymRepository { get; }

        IEpisodesRepository EpisodesRepository { get; }

        IFavoritefRepository FavoritefRepository { get; }

        IFavoritemRepository FavoritemRepository { get; }

        IFilmRepository FilmRepository { get; }

        IMusicRepository MusicRepository { get; }

        ApplicationDbContext ApplicationDbContext { get; }

        Task<int> SaveChanges();
    }
}