﻿using EntertainmentAPI.Data;
using EntertainmentAPI.IRepositories;
using EntertainmentAPI.Repositories;

namespace EntertainmentAPI.Infastructures
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly ApplicationDbContext context;
        private ICategoryDetailRepository categoryDetailRepository;
        private ICategoryfRepository categoryfRepository;
        private ICategorymRepository categorymRepository;
        private IEpisodesRepository episodesRepository;
        private IFavoritefRepository favoritefRepository;
        private IFavoritemRepository favoritemRepository;
        private IFilmRepository filmRepository;
        private IMusicRepository musicRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            this.context = context;
        }

        public ICategoryDetailRepository CategoryDetailRepository => this.categoryDetailRepository ??= new CategoryDetailRepository(this.context);
        
        public ICategoryfRepository CategoryfRepository => this.categoryfRepository ??= new CategoryfRepository(this.context);
        
        public ICategorymRepository CategorymRepository => this.categorymRepository ??= new CategorymRepository(this.context);
        
        public IEpisodesRepository EpisodesRepository => this.episodesRepository ??= new EpisodesRepository(this.context);
        
        public IFavoritefRepository FavoritefRepository => this.favoritefRepository ??= new FavoritefRepository(this.context);
        
        public IFavoritemRepository FavoritemRepository => this.favoritemRepository ??= new FavoritemRepository(this.context);
        
        public IFilmRepository FilmRepository => this.filmRepository ??= new FilmRepository(this.context);
        
        public IMusicRepository MusicRepository => this.musicRepository ??= new MusicRepository(this.context);

        public ApplicationDbContext ApplicationDbContext => this.context;

        public void Dispose()
        {
            this.context.Dispose();
        }

        public async Task<int> SaveChanges()
        {
            return await this.context.SaveChangesAsync();
        }
    }
}
