﻿using EntertainmentAPI.Data.EntityBase;
using System.Linq.Expressions;

namespace EntertainmentAPI.Infastructures
{
    public interface IGenericRepository<TEntity> where TEntity : class, IEntityBase
    {
        Task<IList<TEntity>> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null);

        Task<TEntity> GetById(params object[] keyValues);

        Task Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(params object[] keyValues);

        Task<IList<TEntity>> Find(Expression<Func<TEntity, bool>> condition);
    }
}