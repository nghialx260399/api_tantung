﻿using AutoMapper;
using EntertainmentAPI.Data.Tung;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.ViewModels.CategoriesF;

namespace EntertainmentAPI.Services.CategoriesM
{
    public class CategoriesMService: ICategoriesMService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CategoriesMService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> Create(CategoryFVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Categoriesm>(categoryVm);

                await this.unitOfWork.CategorymRepository.Add(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.CategorymRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<CategoryFVm>> GetAll()
        {
            var categories = await this.unitOfWork.CategorymRepository.GetAll();
            var cagoryVms = new List<CategoryFVm>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<CategoryFVm>(category));
            }
            return cagoryVms;
        }

        public async Task<CategoryFVm> GetById(int id)
        {
            var category = await unitOfWork.CategorymRepository.GetById(id);
            var categoryVm = mapper.Map<CategoryFVm>(category);

            return categoryVm;
        }

        public async Task<bool> Update(CategoryFVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Categoriesm>(categoryVm);

                this.unitOfWork.CategorymRepository.Update(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
