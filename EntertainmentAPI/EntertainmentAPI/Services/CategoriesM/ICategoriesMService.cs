﻿using EntertainmentAPI.ViewModels.CategoriesF;

namespace EntertainmentAPI.Services.CategoriesM
{
    public interface ICategoriesMService
    {
        Task<IList<CategoryFVm>> GetAll();

        Task<bool> Create(CategoryFVm categoryVm);

        Task<CategoryFVm> GetById(int id);

        Task<bool> Update(CategoryFVm categoryVm);

        Task<bool> Delete(int id);
    }
}