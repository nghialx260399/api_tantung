﻿using AutoMapper;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.ViewModels.Episodes;

namespace EntertainmentAPI.Services.Episodes
{
    public class EpisodesService: IEpisodesService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public EpisodesService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> Create(ViewEpisodes categoryVm)
        {
            try
            {
                var category = mapper.Map<Data.Tan.Episodes>(categoryVm);

                await this.unitOfWork.EpisodesRepository.Add(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.EpisodesRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<ViewEpisodes>> GetAll()
        {
            var categories = await this.unitOfWork.EpisodesRepository.GetAll();
            var cagoryVms = new List<ViewEpisodes>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<ViewEpisodes>(category));
            }
            return cagoryVms;
        }

        public async Task<IList<ViewEpisodes>> GetByFilmId(int filmId)
        {
            var categories = await this.unitOfWork.EpisodesRepository.Find(x=>x.FilmsId == filmId);
            var cagoryVms = new List<ViewEpisodes>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<ViewEpisodes>(category));
            }
            return cagoryVms;
        }

        public async Task<ViewEpisodes> GetById(int id)
        {
            var category = await unitOfWork.EpisodesRepository.GetById(id);
            var categoryVm = mapper.Map<ViewEpisodes>(category);

            return categoryVm;
        }

        public async Task<bool> Update(ViewEpisodes categoryVm)
        {
            try
            {
                var category = mapper.Map<Data.Tan.Episodes>(categoryVm);

                this.unitOfWork.EpisodesRepository.Update(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
