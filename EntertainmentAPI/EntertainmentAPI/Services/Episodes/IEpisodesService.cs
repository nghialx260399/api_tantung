﻿using EntertainmentAPI.ViewModels.Episodes;

namespace EntertainmentAPI.Services.Episodes
{
    public interface IEpisodesService
    {
        Task<IList<ViewEpisodes>> GetAll();

        Task<IList<ViewEpisodes>> GetByFilmId(int filmId);

        Task<bool> Create(ViewEpisodes categoryVm);

        Task<ViewEpisodes> GetById(int id);

        Task<bool> Update(ViewEpisodes categoryVm);

        Task<bool> Delete(int id);
    }
}