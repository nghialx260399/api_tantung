﻿using AutoMapper;
using EntertainmentAPI.Data.Tung;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.ViewModels.Musics;

namespace EntertainmentAPI.Services.Musics
{
    public class MusicService: IMusicService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public MusicService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> Create(ViewMusic categoryVm)
        {
            try
            {
                var category = mapper.Map<Data.Tung.Musics>(categoryVm);

                await this.unitOfWork.MusicRepository.Add(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.MusicRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<ViewMusic>> GetAll()
        {
            var categories = await this.unitOfWork.MusicRepository.GetAll();
            var cagoryVms = new List<ViewMusic>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<ViewMusic>(category));
            }
            return cagoryVms;
        }

        public async Task<bool> CreateFavorite(FavoriteM cretateF)
        {
            try
            {
                var favorite = new Favoritem()
                {
                    UserId = cretateF.UserId,
                    MusicId = cretateF.MusicId
                };

                await this.unitOfWork.FavoritemRepository.Add(favorite);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IList<ViewMusic>> GetByAuthor(string author)
        {
            var musics = await this.unitOfWork.MusicRepository.Find(x=>x.Author == author);
            var cagoryVms = new List<ViewMusic>();

            foreach (var category in musics)
            {
                cagoryVms.Add(mapper.Map<ViewMusic>(category));
            }
            return cagoryVms;
        }

        public async Task<IList<ViewMusic>> GetByCategoryId(int categoryId)
        {
            var categories = await this.unitOfWork.MusicRepository.Find(x=>x.CategoryId == categoryId);
            var cagoryVms = new List<ViewMusic>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<ViewMusic>(category));
            }
            return cagoryVms;
        }

        public async Task<ViewMusic> GetById(int id)
        {
            var category = await unitOfWork.MusicRepository.GetById(id);
            var categoryVm = mapper.Map<ViewMusic>(category);

            return categoryVm;
        }

        public async Task<bool> Update(ViewMusic categoryVm)
        {
            try
            {
                var category = mapper.Map<Data.Tung.Musics>(categoryVm);

                this.unitOfWork.MusicRepository.Update(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<ViewMusic>> GetMusicsByFavorite(string userId)
        {
            var favorite = await this.unitOfWork.FavoritemRepository.Find(x => x.UserId == userId);

            var filmVms = new List<ViewMusic>();

            foreach (var item in favorite)
            {
                var film = await this.unitOfWork.MusicRepository.GetById(item.MusicId);
                filmVms.Add(mapper.Map<ViewMusic>(film));
            }
            return filmVms;
        }

        public async Task<IList<ViewMusic>> SearchByName(string name)
        {
            var categories = await this.unitOfWork.MusicRepository.Find(x=>x.Name.ToLower().Contains(name.ToLower()));
            var cagoryVms = new List<ViewMusic>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<ViewMusic>(category));
            }
            return cagoryVms;
        }

        public async Task<bool> DeleteFavorite(string userId, int musicId)
        {
            try
            {
                this.unitOfWork.FavoritemRepository.DeleteFavorite(userId, musicId);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
