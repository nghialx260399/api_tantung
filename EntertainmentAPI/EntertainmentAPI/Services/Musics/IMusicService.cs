﻿using EntertainmentAPI.ViewModels.Musics;

namespace EntertainmentAPI.Services.Musics
{
    public interface IMusicService
    {
        Task<IList<ViewMusic>> GetAll();

        Task<IList<ViewMusic>> SearchByName(string name);

        Task<IList<ViewMusic>> GetByAuthor(string author);

        Task<IList<ViewMusic>> GetMusicsByFavorite(string userId);

        Task<IList<ViewMusic>> GetByCategoryId(int categoryId);

        Task<bool> CreateFavorite(FavoriteM cretateF);

        Task<bool> DeleteFavorite(string userId, int musicId);

        Task<bool> Create(ViewMusic categoryVm);

        Task<ViewMusic> GetById(int id);

        Task<bool> Update(ViewMusic categoryVm);

        Task<bool> Delete(int id);
    }
}