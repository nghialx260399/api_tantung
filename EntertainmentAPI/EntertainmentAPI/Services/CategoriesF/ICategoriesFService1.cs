﻿using EntertainmentAPI.ViewModels.CategoriesF;

namespace EntertainmentAPI.Services.CategoriesF
{
    public interface ICategoriesFService
    {
        Task<IList<CategoryFVm>> GetAll();

        Task<bool> Create(CategoryFVm categoryVm);

        Task<CategoryFVm> GetById(int id);

        Task<bool> Update(CategoryFVm categoryVm);

        Task<bool> Delete(int id);
    }
}