﻿using AutoMapper;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.ViewModels.CategoriesF;

namespace EntertainmentAPI.Services.CategoriesF
{
    public class CategoriesFService: ICategoriesFService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CategoriesFService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<bool> Create(CategoryFVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Categoriesf>(categoryVm);

                await this.unitOfWork.CategoryfRepository.Add(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.CategoryfRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<CategoryFVm>> GetAll()
        {
            var categories = await this.unitOfWork.CategoryfRepository.GetAll();
            var cagoryVms = new List<CategoryFVm>();

            foreach (var category in categories)
            {
                cagoryVms.Add(mapper.Map<CategoryFVm>(category));
            }
            return cagoryVms;
        }

        public async Task<CategoryFVm> GetById(int id)
        {
            var category = await unitOfWork.CategoryfRepository.GetById(id);
            var categoryVm = mapper.Map<CategoryFVm>(category);

            return categoryVm;
        }

        public async Task<bool> Update(CategoryFVm categoryVm)
        {
            try
            {
                var category = mapper.Map<Categoriesf>(categoryVm);

                this.unitOfWork.CategoryfRepository.Update(category);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
