﻿using EntertainmentAPI.ViewModels.Films;

namespace EntertainmentAPI.Services.Films
{
    public interface IFilmService
    {
        Task<IList<ViewFilmVm>> GetAll();

        Task<IList<ViewFilmVm>> SearchByName(string name);

        Task<IList<ViewFilmVm>> GetFilmsByFavorite(string userId);

        Task<IList<ViewFilmVm>> GetFilmsByPosition(string position);

        Task<IList<ViewFilmVm>> GetByCategoryId(int categoryId);

        Task<bool> Create(CreateFilmVm createFilms);

        Task<bool> CreateFavorite(FavoriteF cretateF);

        Task<bool> DeleteFavorite(string userId, int filmId);

        Task<bool> Update(CreateFilmVm viewFilmVm);

        Task<CreateFilmVm> GetById(int id);

        Task<bool> Delete(int id);
    }
}