﻿using AutoMapper;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Infastructures;
using EntertainmentAPI.ViewModels.CategoriesF;
using EntertainmentAPI.ViewModels.Films;

namespace EntertainmentAPI.Services.Films
{
    public class FilmService: IFilmService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public FilmService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public async Task<IList<ViewFilmVm>> GetAll()
        {
            var films = await this.unitOfWork.FilmRepository.GetAll();
            var filmVms = new List<ViewFilmVm>();

            foreach (var film in films)
            {
                filmVms.Add(mapper.Map<ViewFilmVm>(film));
            }
            return filmVms;
        }

        public async Task<CreateFilmVm> GetById(int id)
        {
            var category = await unitOfWork.FilmRepository.GetById(id);
            var categoryVm = mapper.Map<CreateFilmVm>(category);

            var categoryDetails = await this.unitOfWork.CategoryDetailRepository.Find(x => x.FilmId == id);

            foreach (var item in categoryDetails)
            {
                var categoryFvm = mapper.Map<CategoryFVm>(await this.unitOfWork.CategoryfRepository.GetById(item.CategoryId));
                categoryVm.CategoryFVms.Add(categoryFvm);
            }

            return categoryVm;
        }

        public async Task<bool> Create(CreateFilmVm createFilm)
        {
            try
            {
                var film = mapper.Map<Data.Tan.Films>(createFilm);

                await this.unitOfWork.FilmRepository.Add(film);
                await this.unitOfWork.SaveChanges();

                foreach (var item in createFilm.CategoryFVms)
                {
                    var categoryDetail = new CategoriesDetails();
                    categoryDetail.FilmId = film.Id;
                    categoryDetail.CategoryId = item.Id;

                    await this.unitOfWork.CategoryDetailRepository.Add(categoryDetail);

                    await this.unitOfWork.SaveChanges();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Update(CreateFilmVm viewFilmVm)
        {
            try
            {
                var categoryDetails = await this.unitOfWork.CategoryDetailRepository.Find(x=>x.FilmId == viewFilmVm.Id);
                this.unitOfWork.CategoryDetailRepository.DeleteByFilmId(categoryDetails);

                foreach (var item in viewFilmVm.CategoryFVms)
                {
                    var categoryDetail = new CategoriesDetails();
                    categoryDetail.FilmId = viewFilmVm.Id;
                    categoryDetail.CategoryId = item.Id;

                    await this.unitOfWork.CategoryDetailRepository.Add(categoryDetail);

                    await this.unitOfWork.SaveChanges();
                }

                var film = mapper.Map<Data.Tan.Films>(viewFilmVm);

                this.unitOfWork.FilmRepository.Update(film);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                unitOfWork.FilmRepository.Delete(id);
                await unitOfWork.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<ViewFilmVm>> GetByCategoryId(int categoryId)
        {
            var categoryDetails = await this.unitOfWork.CategoryDetailRepository.Find(x=>x.CategoryId == categoryId);

            var films = new List<Data.Tan.Films>();
            foreach (var item in categoryDetails)
            {
                films.Add(await this.unitOfWork.FilmRepository.GetById(item.FilmId));
            }
            var filmVms = new List<ViewFilmVm>();

            foreach (var film in films)
            {
                filmVms.Add(mapper.Map<ViewFilmVm>(film));
            }
            return filmVms;
        }

        public async Task<bool> CreateFavorite(FavoriteF cretateF)
        {
            try
            {
                var favorite = new Favoritesf()
                {
                    UserId = cretateF.UserId,
                    FilmsId = cretateF.FilmId
                };

                await this.unitOfWork.FavoritefRepository.Add(favorite);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IList<ViewFilmVm>> GetFilmsByFavorite(string userId)
        {
            var favorite = await this.unitOfWork.FavoritefRepository.Find(x=>x.UserId == userId);

            var filmVms = new List<ViewFilmVm>();

            foreach (var item in favorite)
            {
                var film = await this.unitOfWork.FilmRepository.GetById(item.FilmsId);
                filmVms.Add(mapper.Map<ViewFilmVm>(film));
            }
            return filmVms;
        }

        public async Task<IList<ViewFilmVm>> GetFilmsByPosition(string position)
        {
            var films = await this.unitOfWork.FilmRepository.Find(x=>x.Position.Equals(position));
            var filmVms = new List<ViewFilmVm>();

            foreach (var film in films)
            {
                filmVms.Add(mapper.Map<ViewFilmVm>(film));
            }
            return filmVms;
        }

        public async Task<IList<ViewFilmVm>> SearchByName(string name)
        {
            var films = await this.unitOfWork.FilmRepository.Find(x=>x.Name.ToLower().Contains(name.ToLower()));
            var filmVms = new List<ViewFilmVm>();

            foreach (var film in films)
            {
                filmVms.Add(mapper.Map<ViewFilmVm>(film));
            }
            return filmVms;
        }

        public async Task<bool> DeleteFavorite(string userId, int filmId)
        {
            try
            {
                this.unitOfWork.FavoritefRepository.DeleteFavorite(userId, filmId);
                await this.unitOfWork.SaveChanges();

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
