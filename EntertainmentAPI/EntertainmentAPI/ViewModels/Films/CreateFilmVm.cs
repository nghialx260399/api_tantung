﻿using EntertainmentAPI.Data.EntityBase;
using EntertainmentAPI.ViewModels.CategoriesF;

namespace EntertainmentAPI.ViewModels.Films
{
    public class CreateFilmVm : EntityBase
    {
        public CreateFilmVm()
        {
            CategoryFVms = new List<CategoryFVm>();
        }
        public string? Name { get; set; }

        public string? Author { get; set; }

        public string? Position { get; set; }

        public int? Year { get; set; }

        public int? View { get; set; }

        public float? Rate { get; set; }

        public byte[]? Image { get; set; }

        public bool? Status { get; set; }

        public int? TotalEP { get; set; }

        public IList<CategoryFVm>? CategoryFVms { get; set; }
    }
}
