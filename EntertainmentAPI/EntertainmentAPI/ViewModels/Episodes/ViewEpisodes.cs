﻿using EntertainmentAPI.Data.EntityBase;

namespace EntertainmentAPI.ViewModels.Episodes
{
    public class ViewEpisodes:EntityBase
    {
        public string? Name { get; set; }

        public string? Source { get; set; }

        public int FilmsId { get; set; }
    }
}
