﻿using AutoMapper;
using EntertainmentAPI.Data.Tan;
using EntertainmentAPI.Data.Tung;
using EntertainmentAPI.ViewModels.CategoriesF;
using EntertainmentAPI.ViewModels.Episodes;
using EntertainmentAPI.ViewModels.Films;
using EntertainmentAPI.ViewModels.Musics;

namespace EntertainmentAPI.ViewModels.AutoMapper
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<Categoriesf, CategoryFVm>().ReverseMap();
            CreateMap<Categoriesm, CategoryFVm>().ReverseMap();
            CreateMap<Data.Tan.Films, CreateFilmVm>().ReverseMap();
            CreateMap<Data.Tan.Films, ViewFilmVm>().ReverseMap();
            CreateMap<Data.Tan.Episodes, ViewEpisodes>().ReverseMap();
            CreateMap<Data.Tung.Musics, ViewMusic>().ReverseMap();
        }
    }
}
