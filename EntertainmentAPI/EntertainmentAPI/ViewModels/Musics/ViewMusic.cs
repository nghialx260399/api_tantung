﻿using EntertainmentAPI.Data.EntityBase;

namespace EntertainmentAPI.ViewModels.Musics
{
    public class ViewMusic:EntityBase
    {
        public string? Name { get; set; }

        public byte[]? Image { get; set; }

        public string? Author { get; set; }

        public string? Time { get; set; }

        public string? Source { get; set; }

        public int CategoryId { get; set; }
    }
}
