﻿using EntertainmentAPI.Data.EntityBase;

namespace EntertainmentAPI.ViewModels.CategoriesF
{
    public class CategoryFVm:EntityBase
    {
        public string? Name { get; set; }
    }
}
