﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EntertainmentAPI.Migrations
{
    public partial class updateTung : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Time",
                table: "Musics",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Time",
                table: "Musics");
        }
    }
}
