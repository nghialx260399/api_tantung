﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EntertainmentAPI.Migrations
{
    public partial class updateTan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Position",
                table: "Films",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Position",
                table: "Films");
        }
    }
}
